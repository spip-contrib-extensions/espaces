<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/espaces.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'espaces_description' => 'Gestion d’espaces',
	'espaces_nom' => 'Espaces',
	'espaces_slogan' => 'Gestion d’espaces'
);
